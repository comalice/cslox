﻿using System.Text;

namespace CSLox
{
    public class Lox {
        public static bool HadError { get; private set; }

        public static void Main(string []args)
        {
            if (args.Length > 0)
            {
                Console.WriteLine("Usage: cslox [script]");
                Environment.Exit(64);
            } else if (args.Length == 1)
            {
                runFile(args[0]);
            } else {
                runPrompt();
            }
        }

        private static void runPrompt()
        {
            var input = new StreamReader(Console.OpenStandardInput(), Console.InputEncoding);

            while (true)
            {
                Console.Write("> ");
                var line = input.ReadLine();
                if (line == null) { break; }
                run(line);
            }
        }

        private static void runFile(string path)
        {
            var bytes = File.ReadAllBytes(Path.GetFullPath(path));
            var encoding = Encoding.GetEncoding("utf-8");
            run(encoding.GetString(bytes));
        }

        private static void run(string source)
        {
            var scanner = new Scanner(source);
            List<Token> tokens = scanner.ScanTokens();

            foreach(var token in tokens)
            {
                Console.WriteLine(token);
            }
        }

        internal static void error(int line, string message)
        {
            report(line, "", message);
        }

        internal static void report(int line, string where, string message)
        {
            Console.WriteLine($"[line: {line}] Error {where}: {message}");
            HadError = true;
        }
    }
}
