﻿namespace CSLox
{
    internal class Scanner
    {
        private static Dictionary<string, TokenType> keywords = new Dictionary<string, TokenType>
        {
            { "and", TokenType.AND },
            { "class", TokenType.CLASS },
            { "else", TokenType.ELSE },
            { "false", TokenType.FALSE },
            { "true", TokenType.TRUE },
            { "for", TokenType.FOR },
            { "fun", TokenType.FUN },
            { "if", TokenType.IF },
            { "nil", TokenType.NIL },
            { "or", TokenType.OR },
            { "print", TokenType.PRINT },
            { "return", TokenType.RETURN },
            { "super", TokenType.SUPER },
            { "this", TokenType.THIS },
            { "true", TokenType.TRUE },
            { "var", TokenType.VAR },
            { "while", TokenType.WHILE },
        };
        private string Source;
        private List<Token> Tokens = new List<Token>();
        private int Start = 0;
        private int Current = 0;
        private int Line = 1;

        public Scanner(string source)
        {
            this.Source = source;
        }

        public List<Token> ScanTokens()
        {
            while (!IsAtEnd())
            {
                // Currently at the beginning of the next lexeme.
                Start = Current;
                ScanToken();
            }

            Tokens.Add(new Token(TokenType.EOF, "", null, Line));
            return Tokens;
        }

        private void ScanToken()
        {
            char c = advance();
            switch (c)
            {
                case '(': addToken(TokenType.LEFT_PAREN); break;
                case ')': addToken(TokenType.RIGHT_PAREN); break;
                case '{': addToken(TokenType.LEFT_BRACE); break;
                case '}': addToken(TokenType.RIGHT_BRACE); break;
                case ',': addToken(TokenType.COMMA); break;
                case '.': addToken(TokenType.DOT); break;
                case '-': addToken(TokenType.MINUS); break;
                case '+': addToken(TokenType.PLUS); break;
                case ';': addToken(TokenType.SEMICOLON); break;
                case '*': addToken(TokenType.STAR); break;
                case '!':
                    addToken(match('=') ? TokenType.BANG_EQUEL : TokenType.BANG);
                    break;
                case '=':
                    addToken(match('=') ? TokenType.EQUAL_EQUAL : TokenType.EQUAL);
                    break;
                case '<':
                    addToken(match('=') ? TokenType.LESS_EQUAL : TokenType.LESS);
                    break;
                case '>':
                    addToken(match('=') ? TokenType.GREATER_EQUAL : TokenType.GREATER);
                    break;
                case '/':
                    if (match('/'))
                    {
                        while (peek() != '\n' && !IsAtEnd()) advance();
                    } else
                    {
                        addToken(TokenType.SLASH);
                    }
                    break;
                case ' ':
                case '\r':
                case '\t':
                    // Ignore whitespace.
                    break;
                case '\n':
                    Line++;
                    break;
                case '"': _string(); break;
                default:
                    if (IsDigit(c))
                    {
                        number();
                    } else if (IsAlpha(c))
                    {
                        identifier();
                    } else
                    {
                        Lox.error(Line, $"Unexpected character {c}.");
                    }
                    break;
            }
        }

        private bool IsAlpha(char c)
        {
            return (c >= 'a' && c <= 'z') ||
                (c >= 'A' && c <= 'Z') ||
                (c == '_');
        }

        private void identifier()
        {
            while (IsAlphanumeric(peek())) advance();

            var text = Source.Substring(Start, Current - Start);
            var type = keywords[text];
            if (type == null) type = TokenType.IDENTIFIER;

            addToken(type);
        }

        private bool IsAlphanumeric(char c)
        {
            return IsAlpha(c) || IsDigit(c);
        }

        private void number()
        {
            while (IsDigit(peek())) advance();

            // Look for a fractional part.
            if (peek() == '.' && IsDigit(peekNext()))
            {
                // Consume the "."
                advance();

                while (IsDigit(peek())) advance();
            }

            addToken(TokenType.NUMBER, Double.Parse(Source.Substring(Start, Current - Start)));
        }

        private char peekNext()
        {
            if (Current + 1 >= Source.Length) return '\0';
            return Source[Current + 1];
        }

        private bool IsDigit(char c)
        {
            return (c >= '0' && c <= '9');
        }

        private void _string()
        {
            while (peek() != '"' && !IsAtEnd())
            {
                if (peek() == '\n') Line++;
                advance();
            }

            if (IsAtEnd())
            {
                Lox.error(Line, "Unterminated string.");
                return;
            }

            // Consume the closing ".
            advance();

            // Trim the surrounding quotes.
            var value = Source.Substring(Start + 1, Current - Start +1);
            addToken(TokenType.STRING, value);

            // If we want to implement escape characters, we'll handle those here.
        }

        private char peek()
        {
            if (IsAtEnd()) return '\0';
            return Source[Current];
        }

        private bool match(char expected)
        {
            if (IsAtEnd()) return false;
            if (Source[Current] != expected) return false;

            Current++;
            return true;
        }

        private void addToken(TokenType type)
        {
            addToken(type, null);
        }

        private void addToken(TokenType type, object literal)
        {
            string text = Source.Substring(Start, Current - Start);
            Tokens.Add(new Token(type, text, literal, Line));
        }

        private char advance()
        {
            return Source[Current++];
        }

        private bool IsAtEnd()
        {
            return Current >= Source.Length;
        }
    }
}