﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cslox.src.Tools
{
    public class GenerateAST
    {
        public static void Main(string[] args)
        {
            if (args.Length != 1)
            {
                Console.WriteLine("Usage: GenerateAST <output directory>");
                Environment.Exit(0);
            }
            var output_dir = args[0];

            defineAST(output_dir, "Expr", new List<string>
            {
                "Binary  : Expr left, Token _operator, Expr right",
                "Grouping : Expr expression",
                "Literal : object value",
                "Unary : Token _operator, Expr right"
            });
        }

        private static void defineAST(string output_dir, string base_name, List<string> types)
        {
            var path = output_dir + "/" + base_name + ".cs";
            var writer = new StreamWriter(path);

            writer.WriteLine("namespace CSLox {");

            // Define the visitor interface.
            defineVisitor(writer, base_name, types);

            writer.WriteLine($"abstract record class {base_name} {{");
            // The base accept() method.
            writer.WriteLine();
            writer.WriteLine("    abstract <T> T accept(Visitor<T> visitor);");
            // Close base_name.
            writer.WriteLine("}");

            foreach (var type in types)
            {
                var class_name = type.Split(":")[0].Trim();
                var fields_list = type.Split(":")[1].Trim();
                defineType(writer, base_name, class_name, fields_list);
            }

            // Close namespace.
            writer.WriteLine("}");
        }

        private static void defineVisitor(StreamWriter writer, string base_name, List<string> types)
        {
            writer.WriteLine("    interface Visitor<T> {");
            foreach (var type in types)
            {
                var type_name = type.Split(":")[0].Trim();
                writer.WriteLine($"    public T Visit{type_name}{base_name}({type_name} {base_name.ToLower()});");
            }
            writer.WriteLine("    }");
        }

        private static void defineType(StreamWriter writer, string base_name, string class_name, string fields_list)
        {
            // Class signature.
            writer.WriteLine($"    internal record class {class_name} : {base_name} {{");

            writer.WriteLine();
            writer.WriteLine("        override <T> T accept(Visitor<T> visitor) {");
            writer.WriteLine($"            return visitor.visit{class_name}{base_name}(this)");
            writer.WriteLine("        }");

            // Fields
            var fields = fields_list.Split(", ");
            foreach (var field in fields)
            {
                writer.WriteLine($"        public {field};");
            }
            writer.WriteLine("    }");
        }
    }
}
