namespace CSLox
{
    public interface Visitor<T>
    {
        T VisitBinaryExpr(Binary<T> expr);
        T VisitGroupingExpr(Grouping<T> expr);
        T VisitLiteralExpr(Literal<T> expr);
        T VisitUnaryExpr(Unary<T> expr);
    }

    public abstract class Expr<T>
    {
        public abstract T Accept(Visitor<T> visitor);
    }

    public class Binary<T> : Expr<T>
    {
        public Binary (Expr<T> left, Token _operator, Expr<T> right) {
            Left = left;
            _Operator = _operator;
            Right = right;
        }

        public override T Accept(Visitor<T> visitor)
        {
            return visitor.VisitBinaryExpr(this);
        }

        public Expr<T> Left;
        public Token _Operator;
        public Expr<T> Right;
    }

    public class Grouping<T> : Expr<T>
    {
        public Grouping(Expr<T> expr) {
            Expression = expr;
        }
        
        public override T Accept(Visitor<T> visitor)
        {
            return visitor.VisitGroupingExpr(this);
        }

        public Expr<T> Expression;
    }

    public class Literal<T> : Expr<T>
    {
        public Literal(object? value)
        {
            Value = value;
        }
        
        public override T Accept(Visitor<T> visitor)
        {
            return visitor.VisitLiteralExpr(this);
        }

        public object? Value;
    }

    public class Unary<T> : Expr<T>
    {
        public Unary (Token _operator, Expr<T> right) {
            _Operator = _operator;
            Right = right;
        }

        public override T Accept(Visitor<T> visitor)
        {
            return visitor.VisitUnaryExpr(this);
        }

        public Token _Operator;
        public Expr<T> Right;
    }
}
