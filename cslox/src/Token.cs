﻿namespace CSLox
{
    public class Token
    {
        public Token(TokenType type, string lexeme, object literal, int line)
        {
            this.Type = type;
            this.Lexeme = lexeme;
            this.Literal = literal;
            this.Line = line;
        }

        public TokenType Type { get; }
        public string Lexeme { get; }

        public object Literal;
        private int Line;

        public override string ToString()
        {
            return $"{Type} {Lexeme} {Literal}";
        }
    }
}
