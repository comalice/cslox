namespace CSLox {
    class Parser<T> {
        private List<Token> tokens;
        private int current = 0;

        Parser(List<Token> tokens) {
            this.tokens = tokens;
        }

        private Expr<T> expression() {
            return equality();
        }

        private Expr<T> equality() {
            Expr<T> expr = comparison();

            while (match(TokenType.BANG_EQUAL, TokenType.EQUAL_EQUAL)) {
                Token _operator = previous();
                Expr<T> right = comparison();
                expr = new Binary<T>(expr, _operator, right);
            }

            return expr;
        }

        private Expr<T> comparison() {
            Expr<T> expr = term();

            while (match(TokenType.GREATER, TokenType.GREATER_EQUAL, TokenType.LESS, TokenType.LESS_EQUAL)) {
                Token _operator = previous();

                Expr<T> right = term();
                expr = new Binary<T>(expr, _operator, right);
            }

            return expr;
        }

        private Expr<T> term() {
            Expr<T> expr = factor();

            while (match(TokenType.MINUS, TokenType.PLUS)) {
                Token _operator = previous();
                Expr right = factor();
                expr = new Binary<T>(expr, _operator, right);
            }

            return expr;
        }

        private Expr<T> factor() {
            Expr<T> expr = unary();

            while (match(TokenType.SLASH, TokenType.STAR)) {
                Token _operator = previous();
                Expr<T> right = unary();
                expr = new Binary<T>(expr, _operator, right);
            }

            return expr;
        }

        private Expr<T> unary() {
            if (match(TokenType.BANG, TokenType.MINUS)) {
                Token _operator = previous();
                Expr<T> right = unary();
                return new Unary<T>(_operator, right);
            }

            return primary();
        }

        private Expr<T> primary() {
            if (match(TokenType.FALSE)) { return new Literal<T>(false); }
            if (match(TokenType.TRUE)) { return new Literal<T>(true); }
            if (match(TokenType.NIL)) { return new Literal<T>(null); }

            if (match(TokenType.NUMBER, TokenType.STRING)) {
                return new Literal<T>(previous().Literal);
            }

            if (match(TokenType.LEFT_PAREN)) {
                Expr<T> expr = expression();
                consume(TokenType.RIGHT_PAREN, "Expect ')' after expression.");

                return new Grouping<T>(expr);
            }
        }

        private bool match(params TokenType[] types) {
            foreach (TokenType type in types) {
                if (check(type)) {
                    advance();
                    return true;
                }
            }

            return false;
        }

        private bool check(TokenType type) {
            if (isAtEnd()) {
                return false;
            }

            return peek().Type == type;
        }

        private Token advance() {
            if (!isAtEnd()) {
                current++;
            }
            return previous();
        }

        private bool isAtEnd() {
            return peek().Type == TokenType.EOF;
        }

        private Token peek() {
            return tokens[current];
        }

        private Token previous() {
            return tokens[current - 1];
        }
    }
}
